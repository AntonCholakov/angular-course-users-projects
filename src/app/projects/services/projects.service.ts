import {Injectable} from '@angular/core';
import {Project} from '../models/project.model';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ProjectsService {

  constructor(private httpClient: HttpClient) {
  }

  getAll(): Observable<Project[]> {
    return this.httpClient.get<Project[]>('http://localhost:3000/projects');
  }

}
