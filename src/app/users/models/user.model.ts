export class User {
  id: number;
  name: string;
  email: string;
  location: string;
  position: string;
  department: string;
  imageUrl: string;
}
