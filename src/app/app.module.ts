import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {UsersComponent} from './users/users.component';
import {ProjectsComponent} from './projects/projects.component';
import {ProjectsService} from './projects/services/projects.service';
import {HttpClientModule} from '@angular/common/http';
import {UsersService} from './users/services/users.service';


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    ProjectsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    ProjectsService,
    UsersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
